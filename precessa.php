
<?php
// Carregar imagem já existente no servidor

include 'Watermark.php';

$arr = [
    'img_main' => $_FILES['image']['tmp_name'],
    'main_type' => $_FILES['image']['type'],
    'new_name' => $_FILES['image']['name'],
    'location' => 'img/',
    'm_height' => $_POST['alturaP'],
    'm_width' => $_POST['larguraP'],
    'watermark' => 'img/marcadagua.jpg',
    'watermark_type' => 'image/jpeg',
    'w_height' => $_POST['largura'],
    'w_width' => $_POST['altura'],
    'positionX' => $_POST['positionX'],
    'positionY' => $_POST['positionY'],
    'opacity' => $_POST['opacity'],
    'quality' => $_POST['quality'],
];

$resizeImg = new Watermark();
$rsp = $resizeImg->resize($arr);
echo '<img src="'.$rsp.'" alt="">';