<?php
// ----------------------------------------
// set size image watermark               |
// ----------------------------------------

if ($_POST['altura'] != '' || $_POST['largura'] != '') {
    # code...
    if ($_POST['altura'] == '') {
        # code...
        $altura = 0;
    } else {
        $altura = $_POST['altura'];
    }
    if ($_POST['largura'] == '') {
        # code...
        $largura = 0;
    } else {
        $largura = $_POST['largura'];
    }
} else {
    $altura = false;
    $largura = false;
}


// ----------------------------------------
// set size image main                    |
// ----------------------------------------
if ($_POST['alturaP'] != '' || $_POST['larguraP'] != '') {
    if ($_POST['alturaP'] == "") {
        $altura_p = 0;
    } else {
        $altura_p = $_POST['alturaP'];
    }
    if ($_POST['larguraP'] == '') {
        $largura_p = 0;
    } else {
        $largura_p = $_POST['larguraP'];
    }
} else {
    $largura_p = false;
    $altura_p = false;
}

// ----------------------------------------
// set position opacit e quality          |
// ----------------------------------------
if ($_POST['opacity'] == '') {
    $opacity = 70;
} else {
    $opacity = $_POST['opacity'];
}
if ($_POST['quality'] == '') {
    $quality = 90;
} else {
    $quality = $_POST['quality'];
}

// ----------------------------------------
// option watermark                       |
// ----------------------------------------


if ($largura !== false) {   

    $marcadagua = imagecreatefromjpeg('img/marcadagua.jpg');
    $largura_original = imagesx($marcadagua);
    $altura_original = imagesy($marcadagua);
    // ----------------------------------------
    // new width                              |
    // ----------------------------------------
    if ($largura == 0) {
        # altura setada
        $largura = ($largura_original / $altura_original) * $altura;

        $nova_largura = $largura ? $largura : floor(($largura_original / $altura_original) * $largura);
    } else {
        $nova_largura = $largura ? $largura : floor(($largura_original / $altura_original) * $largura);
    }
    // ----------------------------------------
    // new height                             |
    // ----------------------------------------
    if ($altura == 0) {
        $altura = ($altura_original / $largura_original) * $largura;
        $nova_altura = $altura ? $altura : floor(($altura_original / $largura_original) * $altura);
    } else {
        $nova_altura = $altura ? $altura : floor(($altura_original / $largura_original) * $altura);
    }

    $img_redim = imagecreatetruecolor($nova_largura, $nova_altura);
    imagecopyresampled($img_redim, $marcadagua, 0, 0, 0, 0, $nova_largura, $nova_altura, $largura_original, $altura_original);

    $largura_marcadagua = imagesx($img_redim);
    $altura_marcadagua = imagesy($img_redim);
}


$name = $_FILES['image']['name'];

if ($_FILES['image']['type'] == 'image/jpeg') {
    $img_temporaria = imagecreatefromjpeg($_FILES['image']['tmp_name']);
}
if ($_FILES['image']['type'] == 'image/pjpeg') {
    $img_temporaria = imagecreatefromjpeg($_FILES['image']['tmp_name']);
}
if ($_FILES['image']['type'] == 'image/png') {
    $img_temporaria = imagecreatefrompng($_FILES['image']['tmp_name']);
}
if ($_FILES['image']['type'] == 'image/gif') {
    $img_temporaria = imagecreatefromgif($_FILES['image']['tmp_name']);
}


$largura_original_p = imagesx($img_temporaria);
$altura_original_p = imagesy($img_temporaria);

if ($largura_p !== false) {
    if ($largura_p == 0) {
        # altura setada
        $largura_p = ($largura_original_p / $altura_original_p) * $altura_p;
        $nova_largura_p = $largura_p ? $largura_p : floor(($largura_original_p / $altura_original_p) * $largura_p);
    } else {
        $nova_largura_p = $largura_p ? $largura_p : floor(($largura_original_p / $altura_original_p) * $largura_p);
    }
    if ($altura_p == 0) {
        # largura setada
        $altura_p = ($altura_original_p / $largura_original_p) * $largura_p;
        $nova_altura_p = $altura_p ? $altura_p : floor(($altura_original_p / $largura_original_p) * $altura_p);
    } else {
        $nova_altura_p = $altura_p ? $altura_p : floor(($altura_original_p / $largura_original_p) * $altura_p);
    }
    $img_temporaria_redmi = imagecreatetruecolor($nova_largura_p, $nova_altura_p);
    imagecopyresampled($img_temporaria_redmi, $img_temporaria, 0, 0, 0, 0, $nova_largura_p, $nova_altura_p, $largura_original_p, $altura_original_p);
}else{
    $img_temporaria_redmi = imagecreatetruecolor($largura_original_p, $altura_original_p);
    imagecopyresampled($img_temporaria_redmi, $img_temporaria, 0, 0, 0, 0, $largura_original_p, $altura_original_p, $largura_original_p, $altura_original_p);
}

$largura_redmi_p = imagesx($img_temporaria_redmi);
$altura_redmi_p = imagesy($img_temporaria_redmi);


if ($_POST['positionX'] == '') {
    $positionX = $largura_redmi_p - $largura;
} else {
    $positionX = $largura_redmi_p - $largura - $_POST['positionX'];
}
if ($_POST['positionY'] == '') {
    $positionY  = $altura_redmi_p - $altura;
} else {
    $positionY  = $altura_redmi_p - $altura - $_POST['positionY'];
}

if ($largura !== false) {
    $x_logo = imagesx($img_temporaria_redmi) - $largura_marcadagua - $positionX;
    $y_logo = imagesy($img_temporaria_redmi) - $altura_marcadagua - $positionY;
    imagecopymerge($img_temporaria_redmi, $img_redim,  $x_logo,  $y_logo, 0, 0, $largura_marcadagua, $altura_marcadagua, $opacity);
}


imagejpeg($img_temporaria_redmi, 'img/' . $name, $quality);

echo '<img src="img/' . $name . '" alt="">';

